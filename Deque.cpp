/* This is the implementation file for the deque, where you will define how the
 * abstract functionality (described in the header file interface) is going to
 * be accomplished in terms of explicit sequences of C++ statements. */

#include "deque.h"
#include <stdio.h>
#include <cassert>

/* NOTE:
 * We will use the following "class invariants" for our data members:
 * 1. data points to a buffer of size cap.
 * 2. data[front_index,...,next_back_index-1] are the valid members of the
 *    deque, where the indexing is *circular*, and works modulo cap.
 * 3. the deque is empty iff front_index == next_back_index, and so we must
 *    resize the buffer *before* size == cap.  */

#ifndef INIT_CAP
#define INIT_CAP 4
#endif

deque::deque()
{
	/* setup a valid, empty deque */
	this->cap = INIT_CAP;
	this->data = new int[cap];
	this->front_index = 0;
	this->next_back_index = 0;
	/* NOTE: the choice of 0 is arbitrary; any valid index works. */
}
/* range constructor */
deque::deque(int* first, int* last)
{
	if (last <= first) return;
	this->cap = last - first + 1;
	this->data = new int[this->cap];
	this->front_index = 0;
	this->next_back_index = this->cap-1;
	for (size_t i = 0; i < this->cap-1; i++) {
		this->data[i] = first[i];
	}
}
/* l-value copy constructor */
 deque::deque(const deque& D){ // copy constructor
        this->cap = D.cap;
	    this->front_index = D.front_index;
	    this->next_back_index = D.next_back_index;
        this->data = new int[D.cap];
        for ( size_t i = 0; i < D.size(); i++ ){
            this->data[i] = D.data[i];
        }
    }
/* r-value copy constructor */
deque::deque(deque&& D){
	/*steal data from D */
	this->data = D.data;
	this->front_index = D.front_index;
	this->next_back_index = D.next_back_index;
	this->cap = D.cap;
	D.data = NULL;
}
    // Kczynzky Deleon
    deque& deque::operator=(deque RHS){ // assignment operator
        size_t temp;
        this->cap = RHS.cap;
        this->front_index = RHS.front_index;
        this->next_back_index = RHS.next_back_index;
        for ( size_t i = 0; i < RHS.size(); i++ ){
            temp = this->data[i];
            this->data[i] = RHS.data[i];
            RHS.data[i] = temp;
        }
        return *this;
    }


deque::~deque()
{
	/* NOTE: to make the r-value copy more efficient, we allowed for the
	 * possibility of data being NULL. */
	if (this->data != NULL) delete[] this->data;
}

int deque::back() const
{
	/* for debug builds, make sure deque is nonempty */
	assert(!empty());

	return this->data[(next_back_index-1)%cap]; /* prevent compiler error. */
}
int deque::front() const
{
	/* for debug builds, make sure deque is nonempty */
	assert(!empty());
	return this->data[front_index]; /* prevent compiler error. */
}
size_t deque::capacity() const
{
	return this->cap - 1;
}

int& deque::operator[](size_t i) const
{
	assert(i < this->size());
	return this->data[(front_index + i) % this->cap];
}

size_t deque::size() const
{
	/* just compute number of elements between front and back, wrapping
	 * around modulo the size of the data array. */
	assert(!empty());
	return (next_back_index-front_index)%cap;
}

bool deque::empty() const
{
	return (next_back_index == front_index);
}

bool deque::needs_realloc()
{
	return ((next_back_index + 1) % cap == front_index);
}

//Simon Yuen
 void deque::push_front(int x){ //Adding to Front
	if ((front_index == 0 && next_back_index == cap-1) || (next_back_index = (front_index-1)%(cap-1))){ // Full?
		return;
	}
	if (front_index == -1){ // empty?
		this->front_index = 0;
		this->next_back_index = 0;
	}else if (front_index == 0){ // at front?
		this->front_index = cap-1;
	}else{ //move prev index
		this->front_index = front_index--;
	}
	this->front_index = x; //insert

}

    //Simon Yuen
 void deque::push_back(int x){ // Adding to Back
	if ((front_index == 0 && next_back_index == cap-1) || (next_back_index = (front_index-1)%(cap-1))){ //Full?
		return;
	}
	if (front_index == -1){ // empty?
		this->front_index = 0;
		this->next_back_index = 0;
	}else if (next_back_index == cap-1){ // last index?
		this->next_back_index = 0;
	}else{ // move next index
		this->next_back_index = next_back_index++;
	}

	this->next_back_index = x; //insert
	next_back_index = (next_back_index+1)%cap;
 }
    // Haneul Jang
    int deque::pop_front()
    {
    assert(!empty());
		front_index=(front_index+1)%cap;
		return 0;
    }

    int deque::pop_back()
    {
    assert(!empty());
		return 0;
    }

    // Kczynzky Deleon
    void deque::clear(){ // clear function
        next_back_index = front_index;
        // when both the front index and next back index are equal, you have an empty deque!
    }

void deque::print(FILE* f) const
{
	for(size_t i=this->front_index; i!=this->next_back_index;
			i=(i+1) % this->cap)
		fprintf(f, "%i ",this->data[i]);
	fprintf(f, "\n");
}